package com.devcamp.api.rainbow_rest_api.controllers;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RainbowController {
    
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbow() {
        ArrayList<String> rainbows = new ArrayList<>();
        String[] listRainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

        for (String rainbow : listRainbows) {
            rainbows.add(rainbow);
        }

        //rainbows = new ArrayList<>(Arrays.asList(listRainbows));

        return rainbows;
    }
}
